# Example Workflows


1. Call fusion variant from a tumor bam and compare the result with a fusion panel in bedpe format.

    ```{bash,eval=FALSE}
    call_fusion lumpy <sample_id> <in.bam> <out.vcf>
    compare_fusion \
        --log-level=INFO \
        --ignore-secondary=AUTO \
        --max-offset=100 \
        --fusion-out-format=vcf \
        --known-fusion-format=bedpe \
        <out.vcf> <panel_fusion.bedpe> <summary.txt>
    ```

2. Call fusion variant from a tumor bam and annotate the result without a fusion panel.

    ```{bash,eval=FALSE}
    call_fusion lumpy <sample_id> <in.bam> <out.vcf>
    compare_fusion \
       --log-level=INFO \
       --ignore-secondary=AUTO \
       --fusion-out-format=vcf \
       <out.vcf> <summary.txt>
    ```
   
3. Call fusion variant from tumor-normal paired bam files and compare the result with a fusion panel in bedpe format.

    ```{bash,eval=FALSE}
    call_fusion novobreak <sample_id> <tumor.bam> <normal.bam> <output_dir>
    compare_fusion \
        --log-level=INFO \
        --ignore-secondary=AUTO \
        --max-offset=100 \
        --fusion-out-format=novobreak \
        --known-fusion-format=bedpe \
    	<output_dir>/novoBreak.pass.flt.vcf <panel_fusion.bedpe> <summary.txt>
    ```
